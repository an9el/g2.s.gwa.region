# g2.s.gwa.region 0.2.0

* Added support for analyze also the miRNA
* Corrected a error when analyze tRNA, with the `as.data.frame` function. If you use base::as.data.frame in a GRranges object, no row.names is preserved and then a error is propagated.
* Added the emojis

# g2.s.gwa.region 0.1.0

* Added the parameter `family` to the functions. Now is possible to use it to calculate the region-based genome wide association studies also with dichotomic traits, as diseases.
* Added also to the README.Rmd the actual analysis made.

# g2.r.gwa.region 0.0.0.9000

* First release of the package
* Added a `NEWS.md` file to track changes to the package.
