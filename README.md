g2.s.gwa.region package
================
:pencil: Angel Martinez-Perez [![ORCID
ID](inst/ORCIDiD_icon24x24.png)](https://orcid.org/0000-0002-5934-1454)





<!-- README.md is generated from README.Rmd. Please edit that file -->

-----

## g2.s.gwa.region

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![License:
MIT](https://img.shields.io/badge/license-MIT-blue.svg)](#license)
[![Last-changedate](https://img.shields.io/badge/last%20change-2021--03--29-yellowgreen.svg)](/commits/master)
<!-- badges: end -->

  - :arrow\_double\_down: [Installation](#installation)
  - :bulb: [Purpose](#purpose)
  - :mag: [Analysis](#analysis)
  - :chart\_with\_upwards\_trend: [Results](#results)
  - :book: [References](#references)

## :arrow\_double\_down: Installation <a name=installation></a>

You can install the released version of *g2.s.gwa.region* package from
[gitlab](https://gitlab.com) with:

``` r
remotes::install_gitlab("an9el/g2.s.gwa.region")
```

## :bulb: Purpose <a name=purpose></a>

The goal of this package is to unify functions to make region-based
association with
[GENESIS](https://www.bioconductor.org/packages/devel/bioc/vignettes/GENESIS/inst/doc/assoc_test.html)
([aggregate assoc with
GENESIS](https://rdrr.io/bioc/GENESIS/man/assocTestAggregate.html) ),
using a family-based study (the **GAIT2** project). Two random effect
will be used, the kinship matrix (which model the genetic pedigree
structure) and the houseHold (which model the common environmental
conditioning factors, possibly hidden, due to cohabitation of people).

The functions included here are project-dependent, i.e. for the use in
another project you will probably have to modify the source code, even
so, it can serve you as a guide.

There are 6 main functions to make 6 region-based analysis:

  - [g2.s.gwa.exon](#exon)
  - [g2.s.gwa.intron](#intron)
  - [g2.s.gwa.rare](#rare) This function allows you to make 8 different
    region-based methods with a possible threshold in MAF.
  - [g2.s.gwa.tRNA](#tRNA)
  - [g2.s.gwa.transcripts](#transcripts)
  - [g2.s.gwa.miRNA](#miRNA)

It uses the
[GenomicFeatures](https://kasperdanielhansen.github.io/genbioconductor/html/GenomicFeatures.html)
package ([another GenomicFeautures
link](https://kasperdanielhansen.github.io/genbioconductor/html/GenomicFeatures.html))
to define this region units, also uses the
`TxDb.Hsapiens.UCSC.hg19.knownGene` R package to annotate the genes in
the `hg19` reference panel.

Finally there is one function to collect the results, and annotate the
top hits:

  - [g2.s.gwa.region.annot](#collect)

## :mag: Example of analysis: <a name=analysis></a>

``` r
## First load the required libraries
suppressPackageStartupMessages({ 
    library(pacman)
    p_load(gait2, GWASTools, tidyverse, g2.s.gwl, magrittr, glue, checkmate, tidyr, rtracklayer, glue,
    SeqVarTools, solarius, mlr, details, data.table, broom, qvalue, qqman, Homo.sapiens,
    GenomicFeatures, TxDb.Hsapiens.UCSC.hg19.knownGene, GENESIS ,SeqArray, Biobase, GenomicRanges,
    SNPRelate, gdsfmt, SKAT, FDb.UCSC.tRNAs, VariantAnnotation, R6, g2.s.gwa.region, knitcitations)
}) 

## Then load the vector with traits and diseases to analyze:
external <- FALSE
f1 <- gait2::g2.s.pheno(external = external)
diseases <- f1$getTraits('normalDiseases')
phenosToAnalize <- f1$getTraits('all.tr')
## discard some phenotypes and prioritize others
phenosToAnalize <- phenosToAnalize[!(phenosToAnalize%>% str_detect('^lnF'))]
phenosToAnalize <- phenosToAnalize[!(phenosToAnalize%>% str_detect('^sysVT'))]
ultimo <- c(phenosToAnalize[(phenosToAnalize %>%
   str_detect('^CD'))], phenosToAnalize[(phenosToAnalize %>% str_detect('_'))])
phenosToAnalize <- c(setdiff(phenosToAnalize,ultimo),ultimo)
miRNAs <- f1$getTraits('fase2Alba')
```

### tRNA analysis <a name=tRNA></a>

Transfer RNA(tRNA) genes in DNA sequence represent a major class of RNA
molecules. They are among the most highly transcribed genes in the
genome owing to their central role in protein synthesis. They are small
(\~70-80 bp) and are present in all known forms of life
<a name=cite-Pinkard2020></a>([Pinkard, McFarland, Sweet, and Coller,
2020](#bib-Pinkard2020)). Their primary function is to help decode a
messenger RNA (mRNA) sequence in order to synthesize protein and thus
ensures the precise translation of genetic information that is imprinted
in DNA. tRNAs also plays regulatory roles
<a name=cite-Raina2014></a>([Raina and Ibba, 2014](#bib-Raina2014)).

[Here](http://gtrnadb.ucsc.edu/Hsapi19/Hsapi19-gene-list.html) is a list
of the high confidence tRNA gene sets.

To learn more about tRNA visit this
[web](https://www.khanacademy.org/science/biology/gene-expression-central-dogma/translation-polypeptides/a/trna-and-ribosomes)

For example to calculate the `tRNA`analysis for all phenotypes of
`GAIT2`project you can use this source code:

``` r

for(j in phenosToAnalize){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.tRNA(chr = i, pheno = j, MAF.max = 1, normalize = TRUE,
      outdir = "g2.r.gwtRNA", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
## for diseases:
for(j in diseases){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.tRNA(chr = i, pheno = j, MAF.max = 1, family = 'binomial', normalize = FALSE,
      outdir = "g2.r.gwtRNA", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
```

### transcripts analysis <a name=transcripts></a>

A classical view of a gene like unitary regions of DNA sequence,
separated from each other is replaced by a simplistic collections of
introns(untranslated regions) and exons(bricks of the translated
regions). Usually in one tissue in a specific time only one
transcript(some specific combination of exons) is expressed. This
concept of “gene” obsolete to look for association with the traits
because, what is finally expressed is not the gene but a particular
transcript. Note that one single mutation can affect multiple
transcripts, and some others may remain unchanged and its function
intact. A way to see this, is to consider that while transcripts exist,
genes don’t.

Gene definition of EnsEMBL:

> The gene is a union of genomic sequences encoding a coherent set of
> potentially overlapping functional products.

The project [**ENCODE**](https://www.encodeproject.org/) unravel a much
more complex picture.

With the gene regulation, the overlapping (some genes can share the same
DNA sequence in a different reading frame or even in the opposite
strand), the splicing ( a gene is a set of different connected exons),
the protein trans-splicing (when the exons can be in two separate mRNA
molecules, on the opposite DNA strand, or even another chromosome and
being joined before being spliced
<a name=cite-Gerstein2007></a>([Gerstein, Bruce, Rozowsky, Zheng, et
al., 2007](#bib-Gerstein2007))), the retrogenes (RNA to DNA flow of
information), transcribed pseudogenes (biochemical activity of
supposedly inactive elements), the transposons (genes that can jump from
one location to another, whose only function seems to replicate
themselves), and tandem chimerism (where two consecutive genes are
transcribed into a single RNA), un-annotated and alternative TSSs
(Transcription Start Sites), made not sense to analyze a compacted DNA
sequence usually called gene. It almost seems that it has blurred the
distinction between intergenic and intragenic regions.

``` r
for(j in phenosToAnalize){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.transcripts(chr = i, pheno = j, MAF.max = 1,
      normalize = TRUE, outdir = "g2.r.transcripts",
      external = FALSE, tests = NULL, verbose = FALSE)
    }
}
## for diseases
for(j in diseases){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.transcripts(chr = i, pheno = j, MAF.max = 1,
      family = 'binomial', normalize = FALSE, outdir = "g2.r.transcripts",
      external = FALSE, tests = NULL, verbose = FALSE)
    }
}
```

### microRNA analysis <a name=miRNA></a>

As microRNA have length with range (49 - 150), more or less, the
probability of having variants there is limited, in most cases is not
possible to make the analysis. Even so we can try and have the results
when is possible.

:fire: :exclamation: The reader always have to assess the validity of
the results, mainly based on the number of variants within that region
and the number of sample with alternate alleles.

``` r

for(j in phenosToAnalize){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.miRNA(chr = i, pheno = j, MAF.max = 1, normalize = TRUE,
      outdir = "g2.r.gwmiRNA", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
for(j in diseases){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.miRNA(chr = i, pheno = j, MAF.max = 1, family = 'binomial', normalize = FALSE,
      outdir = "g2.r.gwmiRNA", external = FALSE, tests = NULL, verbose = FALSE)
    }
}

## Lets explore also the crude association between variants in the miRNA region and the miRNA expression.
for(j in miRNAs){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.miRNA(chr = i, pheno = j, MAF.max = 1, normalize = TRUE,
      outdir = "g2.r.gwmiRNA", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
```

### intron analysis <a name=intron></a>

``` r

for(j in phenosToAnalize){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.intron(chr = i, pheno = j, MAF.max = 1, normalize = TRUE,
      outdir = "g2.r.gwintron", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
for(j in diseases){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.intron(chr = i, pheno = j, MAF.max = 1, family = 'binomial', normalize = FALSE,
      outdir = "g2.r.gwintron", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
```

### exon analysis <a name=exon></a>

``` r
for(j in phenosToAnalize){    
  for(i in as.integer(c(1:22))){
    g2.s.gwa.exon(chr = i, pheno = j, MAF.max = 1, normalize = TRUE,
      outdir = "g2.r.gwexon", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
## also the diseases
for(j in diseases){    
  for(i in as.integer(c(1:22))){
    g2.s.gwa.exon(chr = i, pheno = j, MAF.max = 1, family = 'binomial', normalize = FALSE,
      outdir = "g2.r.gwexon", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
```

## rare analysis <a name=rare></a>

With this function you can perform multiple collapsing analysis changing
the parameter `type`:

``` 
 * genes
 * cds
 * exons
 * miRNAs
 * tRNAs
 * promoters
 * exonicParts
 * intronicParts
 
```

With the parameter `MAF.max = 1` it uses all variants, if you use
`MAF.max = 0.05` (only variants with MAF \<= 0.05 will be used), meaning
rare variant analysis will be performed with the selected collapsing
method.

``` r
## A normal gen-based burden analysis
for(j in phenosToAnalize){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.rare(chr = i, pheno = j, type = 'genes', MAF.max = 1, normalize = TRUE,
      outdir = "g2.r.gen", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
## same for diseases
for(j in diseases){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.rare(chr = i, pheno = j, type = 'genes', MAF.max = 1, normalize = FALSE,
      family = 'binomial', outdir = "g2.r.gen", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
## A rare gen-based burden analysis with a binomial trait
for(j in diseases){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.rare(chr = i, pheno = j, type = 'genes', MAF.max = 0.05, normalize = FALSE,
      family = 'binomial', outdir = "g2.r.gwa.genes.rare", external = FALSE,
        tests = NULL, verbose = FALSE)
    }
}
## A promotes-based burden analysis with a binomial trait
for(j in diseases){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.rare(chr = i, pheno = j, type = 'promoters', MAF.max = 1, normalize = FALSE,
      family = 'binomial', outdir = "g2.r.promoters", external = FALSE,
        tests = NULL, verbose = FALSE)
    }
}
## A promotes-based burden analysis with a cuantitative trait
for(j in miRNAs){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.rare(chr = i, pheno = j, type = 'promoters', MAF.max = 1, normalize = TRUE,
              outdir = "g2.r.promoters", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
## A promotes-based burden analysis with a cuantitative trait
for(j in phenosToAnalize[351:386]){
  for(i in as.integer(c(1:22))){
    g2.s.gwa.rare(chr = i, pheno = j, type = 'promoters', MAF.max = 1, normalize = TRUE,
              outdir = "g2.r.promoters", external = FALSE, tests = NULL, verbose = FALSE)
    }
}
```

# :chart\_with\_upwards\_trend: Results <a name=results></a>

Normally the results will have the following items:

  - *seqnames:* The chromosome value
  - *start:* The base pair start position of the window
  - *end:* The base pair end position of the window
  - *TSS:* The base pair of the Transcription Start Site
  - *strand:* The strand of the unit considered
  - *pheno:* The name of the trait of the association result
  - *n.site:* The number of variant sites included in the test.
  - *n.alt:* The number of alternate alleles included in the test.
  - *n.sample.alt:* The number of samples with an observed alternate
    allele at any variant in the aggregate set.
  - *Score:* The value of the score function
  - *Score.SE:* The estimated standard error of the Score
  - *Score.Stat:* The score Z test statistic
  - *Score.pval:* The score p-value
  - *Est:* An approximation of the effect size estimate for each
    additional unit of burden
  - *Est.SE:* An approximation of the standard error of the effect size
    estimate
  - *PVE:* An approximation of the proportion of phenotype variance
    explained
  - *err:* Takes value 1 if there was an error in calculating the
    p-value.
  - *pval\_burden:* The burden test p-value
  - *pval\_theta:* The p-value of the adjusted SKAT test (which is
    asymptotically independent of the burden test)
  - *pval\_SMMAT:* The SMMAT p-value after combining pval\_burden and
    pval\_theta using Fisher’s method.
  - *variant.id:* The variant ID
  - *pos:* The base pair position
  - *allele.index:* The index of the alternate allele. For biallelic
    variants, this will always be 1.
  - *n.obs:* The number of samples with non-missing genotypes
  - *freq:* The estimated alternate allele frequency
  - *MAC:* The minor allele count. For multi-allelic variants, “minor”
    is determined by comparing the count of the alternate allele
    specified by ‘allele.index’ with the sum of all other alleles.

:exclamation:

> To interpret the results you must take special consideration with the
> items **n.alt, n.sample.alt, Est.SE** and **Score.SE**, if the first
> two are too low or the second two are too high it may imply that the
> results are numerically wrong, and the positives are actually false
> positives.

## Collecting the results <a name=collect></a>

:exclamation: In the manhattan output of this function only regions with
more than 9 observed alternate allele at any variants in the aggregate
set are shown

:exclamation: In the manhattan output of this function only regions with
**Est.SE \< 10** are plotted. Probably regions with bigger **Est.SE**
are errors (for example 2 variants with no variation at all, only the
reference allele are changed). Be careful, in the returned data.frame
all the results are returned (also the wrong ones), and you have to
exclude that.

The results will be stored in one folder split by chromosome and trait.
To gather this results into one single data.frame we use:

### Collect gen-based results

``` r
outDir <- 'g2.r.gwa.region'
dir.create(outDir, showWarnings = FALSE)
## Gather the gen results
g2.r.gwa.gen <- g2.s.gwa.region.annot("g2.r.gwa.gen" ,
  manhattanF = glue::glue("{outDir}/g2.p.gwa.gen.jpeg"),
  pattern = "g2.r.gwa.genes.all.chr.", pvalue = 1e-8)
save(g2.r.gwa.gen, file = glue::glue("{outDir}/g2.r.gwa.gen.RData"))
ps <- qvalue::qvalue(g2.r.gwa.gen$pval_burden, fdr.level = 0.1)
print(summary(ps))        
```

### Collect transcripts results

``` r
outDir <- 'g2.r.gwa.region'
dir.create(outDir, showWarnings = FALSE)
## Gather the transcripts results
g2.r.gwa.transcript <- g2.s.gwa.region.annot("g2.r.transcripts" ,
  manhattanF = glue::glue("{outDir}/g2.p.gwa.transcript.jpeg"),
  pattern = "g2.r.gwa.transcripts.all.chr.", pvalue = 1e-8)
save(g2.r.gwa.transcript, file = glue::glue("{outDir}/g2.r.gwa.transcript.RData"))
ps <- qvalue::qvalue(g2.r.gwa.transcript$pval_burden, fdr.level = 0.1)
print(summary(ps))        
```

### Collect intron results

``` r
outDir <- 'g2.r.gwa.region'
dir.create(outDir, showWarnings = FALSE)
## Gather the intron results
g2.r.gwa.intron <- g2.s.gwa.region.annot("g2.r.gwintron" ,
  manhattanF = glue::glue("{outDir}/g2.p.gwa.intron.jpeg"),
  pattern = "g2.r.gwa.intron.all.chr.", pvalue = 1e-8)
save(g2.r.gwa.intron, file = glue::glue("{outDir}/g2.r.gwa.intron.RData"))
ps <- qvalue::qvalue(g2.r.gwa.intron$pval_burden, fdr.level = 0.1)
print(summary(ps))        
```

### Collect exon results

``` r
## Gather the intron results
g2.r.gwa.exon <- g2.s.gwa.region.annot("g2.r.gwexon" ,
  manhattanF = glue::glue("{outDir}/g2.p.gwa.exon.jpeg"),
  pattern = "g2.r.gwa.exon.all.chr.", pvalue = 1e-8)
save(g2.r.gwa.exon, file = glue::glue("{outDir}/g2.r.gwa.exon.RData"))
ps <- qvalue::qvalue(g2.r.gwa.exon$pval_burden, fdr.level = 0.1)
print(summary(ps))        
```

### Collect tRNA results

``` r
outDir <- 'g2.r.gwa.region'
dir.create(outDir, showWarnings = FALSE)
## Gather the tRNA gene results
g2.r.tRNA <- g2.s.gwa.region.annot("g2.r.gwtRNA" ,
  manhattanF = glue::glue("{outDir}/g2.p.gwa.tRNA.jpeg"),
  pattern = "g2.r.gwa.tRNA.all.chr.", pvalue = 1e-8)
save(g2.r.tRNA, file = glue::glue("{outDir}/g2.r.gwa.tRNA.RData"))
ps <- qvalue::qvalue(g2.r.tRNA$pval_burden, fdr.level = 0.1)
print(summary(ps))        
```

### Collect miRNA results

``` r
## Gather the miRNA results
g2.r.gwa.miRNA <- g2.s.gwa.region.annot("g2.r.gwmiRNA" ,
  manhattanF = glue::glue("{outDir}/g2.p.gwa.miRNA.jpeg"),
  pattern = "g2.r.gwa.miRNA.all.chr.", pvalue = 1e-8)
save(g2.r.gwa.miRNA, file = glue::glue("{outDir}/g2.r.gwa.miRNA.RData"))
ps <- qvalue::qvalue(g2.r.gwa.miRNA$pval_burden, fdr.level = 0.1)
print(summary(ps))        
```

## Filtering results

With this function you can restrict results with the following filters:

  - pheno, to one or more traits
  - pval\_burden, with a maximum p-value
  - Est.SE, with less than some value of standard error of the effect
    size
  - n.site, with a minimum number of variant sites included in the test
  - n.alt, with a minimum number of alternate alleles included in the
    test
  - err with no errors in calculating the p-value

Let’s calculate the FDR \< 0.1 and the qvalue, for the results that are
likely to be real.

``` r

objetos <- c('gen','miRNA','transcript','tRNA','exon','intron','promoters')
dir.create('g2.r.gwr.clean005')
for(i in objetos){
      res <- glue("{g2.s.dirs('res.gwr')}/g2.r.gwa.{i}.RData") %>% getobj
      ## remove unreal tests
      print('-----------------------')
      print(i)      
      resclean <- g2.s.gwr.clean(res,
               pval_burden = 'finite',
           Est.SE = 10,
           n.site = 2L,
           n.alt = 5,
           MAC = 5,
           err = 1)
      rm(res)
      ## calculate FDR < 0.1 and qvalue
      rescleanQvalue <- qvalue(p = resclean$pval_burden, lambda = 0, fdr.level = 0.1)
      resclean$qvalue_burden <- rescleanQvalue$qvalues
      resclean$fdr_burden <- rescleanQvalue$lfdr
      resclean %>%  save(file = glue("g2.r.gwr.clean005/g2.r.gwr.{i}.clean.RData"))
      ## print how many significant test there are at fdr < 0.1
      print(table(rescleanQvalue$significant))
      rm(resclean, rescleanQvalue)
}
```

It also return a summary of the results removed for each incremental
filter.

<a name=references></a>

# :book:

<details open>

<summary> <span title="Click to Collapse/Expand"> REFERENCES </span>
</summary>

<a name=bib-Gerstein2007></a>[\[1\]](#cite-Gerstein2007) M. B. Gerstein,
C. Bruce, J. S. Rozowsky, D. Zheng, et al. “What is a Gene, Post-ENCODE?
History and Updated Definition”. In: *Genome Research* (2007). DOI:
[10.1101/gr.6339607](https://doi.org/10.1101%2Fgr.6339607). URL:
<https://pubmed.ncbi.nlm.nih.gov/17567988/>.

<a name=bib-Pinkard2020></a>[\[2\]](#cite-Pinkard2020) O. Pinkard, S.
McFarland, T. Sweet, and J. Coller. “Quantitative tRNA-sequencing
uncovers metazoan tissue-specific tRNA regulation”. In: *Nature
Communications* 11.1 (2020), pp. 1-15. URL:
<https://www.nature.com/articles/s41467-020-17879-x>.

<a name=bib-Raina2014></a>[\[3\]](#cite-Raina2014) M. Raina and M. Ibba.
“tRNAs as regulators of biological processes”. Eng. In: *Frontiers in
genetics* 5 (jun. 2014), pp. 171-171. ISSN: 1664-8021. URL:
<https://pubmed.ncbi.nlm.nih.gov/24966867>.

</details>

<br>

## :copyright: MIT License <a name=license></a>

> Copyright (c) 2020 Angel Martinez-Perez
> 
> Permission is hereby granted, free of charge, to any person obtaining
> a copy of this software and associated documentation files (the
> “Software”), to deal in the Software without restriction, including
> without limitation the rights to use, copy, modify, merge, publish,
> distribute, sublicense, and/or sell copies of the Software, and to
> permit persons to whom the Software is furnished to do so, subject to
> the following conditions:
> 
> The above copyright notice and this permission notice shall be
> included in all copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
> IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
> CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
> TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
> SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

<details closed>

<summary> <span title="Clik to Expand/Collapse"> Current session info
</span> </summary>

``` r

─ Session info ───────────────────────────────────────────────────────────────
 setting  value                       
 version  R version 4.0.4 (2021-02-15)
 os       Debian GNU/Linux 10 (buster)
 system   x86_64, linux-gnu           
 ui       X11                         
 language (EN)                        
 collate  en_US.UTF-8                 
 ctype    en_US.UTF-8                 
 tz       Europe/Madrid               
 date     2021-03-29                  

─ Packages ───────────────────────────────────────────────────────────────────
 ! package              * version  date       lib
   an9elutils           * 0.1.0    2021-03-29 [2]
   AnnotationDbi          1.52.0   2020-10-27 [2]
   askpass                1.1      2019-01-13 [2]
   assertthat             0.2.1    2019-03-21 [2]
   backports              1.2.1    2020-12-09 [2]
   bibtex                 0.4.2.3  2020-09-19 [2]
   Biobase                2.50.0   2020-10-27 [2]
   BiocFileCache          1.14.0   2020-10-27 [2]
   BiocGenerics           0.36.0   2020-10-27 [2]
   BiocParallel           1.24.1   2020-11-06 [2]
   biomaRt                2.46.3   2021-02-09 [2]
   Biostrings             2.58.0   2020-10-27 [2]
   bit                    4.0.4    2020-08-04 [2]
   bit64                  4.0.5    2020-08-30 [2]
   bitops                 1.0-6    2013-08-17 [2]
   blob                   1.2.1    2020-01-20 [2]
   broom                  0.7.5    2021-02-19 [2]
   cachem                 1.0.4    2021-02-13 [2]
   callr                  3.5.1    2020-10-13 [2]
   cellranger             1.1.0    2016-07-27 [2]
   checkmate              2.0.0    2020-02-06 [2]
   cli                    2.3.1    2021-02-23 [2]
   clipr                  0.7.1    2020-10-08 [2]
   codetools              0.2-18   2020-11-04 [4]
   colorspace             2.0-0    2020-11-11 [2]
   commonmark             1.7      2018-12-01 [2]
   conquer                1.0.2    2020-08-27 [2]
   crayon                 1.4.1    2021-02-08 [2]
   curl                   4.3      2019-12-02 [2]
   data.table             1.14.0   2021-02-21 [2]
   DBI                    1.1.1    2021-01-15 [2]
   dbplyr                 2.1.0    2021-02-03 [2]
   debugme                1.1.0    2017-10-22 [2]
   DelayedArray           0.16.2   2021-02-26 [2]
   desc                   1.3.0    2021-03-05 [2]
   details                0.2.1    2020-01-12 [2]
   devtools               2.3.2    2020-09-18 [2]
   digest                 0.6.27   2020-10-24 [2]
   DNAcopy                1.64.0   2020-10-27 [2]
   dplyr                * 1.0.5    2021-03-05 [2]
   ellipsis               0.3.1    2020-05-15 [2]
   evaluate               0.14     2019-05-28 [2]
   fansi                  0.4.2    2021-01-15 [2]
   fastmap                1.1.0    2021-01-25 [2]
   filehash               2.4-2    2019-04-17 [2]
   forcats              * 0.5.1    2021-01-27 [2]
   foreach                1.5.1    2020-10-15 [2]
   formula.tools          1.7.1    2018-03-01 [2]
   fs                     1.5.0    2020-07-31 [2]
 P g2.s.gwa.region      * 0.2.0    2020-09-25 [?]
   gait2                  0.5.0    2021-02-09 [2]
   gdsfmt                 1.26.1   2020-12-22 [2]
   generics               0.1.0    2020-10-31 [2]
   GENESIS                2.20.1   2021-01-28 [2]
   GenomeInfoDb           1.26.4   2021-03-10 [2]
   GenomeInfoDbData       1.2.4    2020-11-09 [2]
   GenomicAlignments      1.26.0   2020-10-27 [2]
   GenomicFeatures        1.42.2   2021-03-12 [2]
   GenomicRanges          1.42.0   2020-10-27 [2]
   ggplot2              * 3.3.3    2020-12-30 [2]
   glue                   1.4.2    2020-08-27 [2]
   gtable                 0.3.0    2019-03-25 [2]
   GWASExactHW            1.01     2013-01-05 [2]
   GWASTools              1.36.0   2020-10-27 [2]
   haven                  2.3.1    2020-06-01 [2]
   hms                    1.0.0    2021-01-13 [2]
   htmltools              0.5.1.1  2021-01-22 [2]
   htmlwidgets            1.5.3    2020-12-10 [2]
   httr                   1.4.2    2020-07-20 [2]
   IRanges                2.24.1   2020-12-12 [2]
   iterators              1.0.13   2020-10-15 [2]
   jsonlite               1.7.2    2020-12-09 [2]
   knitcitations        * 1.0.12   2021-01-10 [2]
   knitr                  1.31     2021-01-27 [2]
   lattice                0.20-41  2020-04-02 [4]
   lifecycle              1.0.0    2021-02-15 [2]
   lmtest                 0.9-38   2020-09-09 [2]
   logistf                1.24     2020-09-16 [2]
   lubridate              1.7.10   2021-02-26 [2]
   magrittr             * 2.0.1    2020-11-17 [2]
   Matrix                 1.3-2    2021-01-06 [4]
   MatrixGenerics         1.2.1    2021-01-30 [2]
   MatrixModels           0.5-0    2021-03-02 [2]
   matrixStats            0.58.0   2021-01-29 [2]
   memoise                2.0.0    2021-01-26 [2]
   mgcv                   1.8-34   2021-02-16 [4]
   mice                   3.13.0   2021-01-27 [2]
   modelr                 0.1.8    2020-05-19 [2]
   munsell                0.5.0    2018-06-12 [2]
   nlme                   3.1-152  2021-02-04 [2]
   openssl                1.4.3    2020-09-18 [2]
   operator.tools         1.6.3    2017-02-28 [2]
   pillar                 1.5.1    2021-03-05 [2]
   pkgbuild               1.2.0    2020-12-15 [2]
   pkgconfig              2.0.3    2019-09-22 [2]
   pkgload                1.2.0    2021-02-23 [2]
   plyr                   1.8.6    2020-03-03 [2]
   png                    0.1-7    2013-12-03 [2]
   prettyunits            1.1.1    2020-01-24 [2]
   processx               3.4.5    2020-11-30 [2]
   progress               1.2.2    2019-05-16 [2]
   ps                     1.6.0    2021-02-28 [2]
   purrr                * 0.3.4    2020-04-17 [2]
   quantreg               5.85     2021-02-24 [2]
   quantsmooth            1.56.0   2020-10-27 [2]
   qvalue                 2.22.0   2020-10-27 [2]
   R6                     2.5.0    2020-10-28 [2]
   rappdirs               0.3.3    2021-01-31 [2]
   Rcpp                   1.0.6    2021-01-15 [2]
   RCurl                  1.98-1.2 2020-04-18 [2]
   readr                * 1.4.0    2020-10-05 [2]
   readxl                 1.3.1    2019-03-13 [2]
   RefManageR             1.3.0    2020-11-13 [2]
   remotes                2.2.0    2020-07-21 [2]
   reprex                 1.0.0    2021-01-27 [2]
   reshape2               1.4.4    2020-04-09 [2]
   rlang                  0.4.10   2020-12-30 [2]
   rmarkdown              2.7      2021-02-19 [2]
   roxygen2               7.1.1    2020-06-27 [2]
   rprojroot              2.0.2    2020-11-15 [2]
   Rsamtools              2.6.0    2020-10-27 [2]
   RSQLite                2.2.4    2021-03-12 [2]
   rstudioapi             0.13     2020-11-12 [2]
   rtracklayer            1.50.0   2020-10-27 [2]
   rvest                  1.0.0    2021-03-09 [2]
   S4Vectors              0.28.1   2020-12-09 [2]
   sandwich               3.0-0    2020-10-02 [2]
   scales                 1.1.1    2020-05-11 [2]
   SeqArray               1.30.0   2020-10-27 [2]
   SeqVarTools            1.28.1   2020-11-20 [2]
   sessioninfo            1.1.1    2018-11-05 [2]
   SNPRelate              1.24.0   2020-10-27 [2]
   SparseM                1.81     2021-02-18 [2]
   stringi                1.5.3    2020-09-09 [2]
   stringr              * 1.4.0    2019-02-10 [2]
   SummarizedExperiment   1.20.0   2020-10-27 [2]
   survival               3.2-9    2021-03-14 [2]
   testthat               3.0.2    2021-02-14 [2]
   tibble               * 3.1.0    2021-02-25 [2]
   tidyr                * 1.1.3    2021-03-03 [2]
   tidyselect             1.1.0    2020-05-11 [2]
   tidyverse            * 1.3.0    2019-11-21 [2]
   tikzDevice           * 0.12.3.1 2020-06-30 [2]
   usethis                2.0.1    2021-02-10 [2]
   utf8                   1.2.1    2021-03-12 [2]
   vctrs                  0.3.7    2021-03-29 [2]
   visNetwork             2.1.0    2021-02-05 [2]
   withr                  2.4.1    2021-01-26 [1]
   xfun                   0.22     2021-03-11 [2]
   XML                    3.99-0.5 2020-07-23 [2]
   xml2                   1.3.2    2020-04-23 [2]
   XVector                0.30.0   2020-10-27 [2]
   yaml                   2.2.1    2020-02-01 [2]
   zlibbioc               1.36.0   2020-10-27 [2]
   zoo                    1.8-9    2021-03-09 [2]
 source                                    
 gitlab (an9el/an9elutils@a6c67e5)         
 Bioconductor                              
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.2)                            
 Bioconductor                              
 Bioconductor                              
 Bioconductor                              
 Bioconductor                              
 Bioconductor                              
 Bioconductor                              
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 Bioconductor                              
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.2)                            
 Bioconductor                              
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.2)                            
 gitlab (an9el/g2.s.gwa.region@de00fa9)    
 gitlab (an9el/gait2@d3b6af2)              
 Bioconductor                              
 CRAN (R 4.0.2)                            
 Bioconductor                              
 Bioconductor                              
 Bioconductor                              
 Bioconductor                              
 Bioconductor                              
 Bioconductor                              
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 Bioconductor                              
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.2)                            
 Bioconductor                              
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.4)                            
 Bioconductor                              
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.4)                            
 Bioconductor                              
 Bioconductor                              
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 Bioconductor                              
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.3)                            
 Bioconductor                              
 CRAN (R 4.0.4)                            
 Bioconductor                              
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.1)                            
 Bioconductor                              
 Bioconductor                              
 CRAN (R 4.0.1)                            
 Bioconductor                              
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.1)                            
 Bioconductor                              
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.1)                            
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.4)                            
 Github (datastorm-open/visNetwork@d91c707)
 CRAN (R 4.0.3)                            
 CRAN (R 4.0.4)                            
 CRAN (R 4.0.2)                            
 CRAN (R 4.0.1)                            
 Bioconductor                              
 CRAN (R 4.0.1)                            
 Bioconductor                              
 CRAN (R 4.0.4)                            

[1] /home/amartinezp/R/x86_64-pc-linux-gnu-library/4.0
[2] /usr/local/lib/R/site-library
[3] /usr/lib/R/site-library
[4] /usr/lib/R/library

 P ── Loaded and on-disk path mismatch.
```

</details>

<br>
