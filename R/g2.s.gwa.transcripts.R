
##' perform transcript association test using GENESIS with mixed models and family relationships and Household status as random effects
##'
##'
##' Using ‘GenomicFeatures’, we obtain which exons makes up which transcripts. With each group of exons that make up a transcript, we associate the group of variants within this region (not consecutive) with a burden test.
##'
##' Burden test, is always returned, but also you can specify 'SMMAT','SKATO' or 'fastSKAT'
##' 
##' 
##' @title perform aggregate association test using GENESIS
##' @param chr  integer chromosome 1L to 22L 
##' @param pheno character, name of the phenotype to analyze.
##' @param MAF.max numeric to specify the maximum MAF allowed in the analysis, 0.05 for rare variant analysis
##' @param family character to specify the error distribution to be used, see `family` for further options
##' @param normalize logical, should the phenotype be inverse normalized?
##' @param outdir character, folder to put the  results, if not exist is created
##' @param visualizeResults logical, should a plot be produced?
##' @param external logical, parameter used by gait2 library, to locate source gds
##' @param tests if NULL only Burden test will performed, if indicate also the other test will be performed
##' @param verbose logical to indicate if should display information
##' @return if verbose = TRUE return the data.frame with the results, if FALSE return invisible()
##' @author person("Angel", "Martinez-Perez", email = "angelmartinez@protonmail.com", role = c("aut", "cre"), comment = c(ORCID = "0000-0002-5934-1454"))
##' @keywords GAIT2, analysis, gwa, region-based test
##' @note 2020-07-15. 
##' @examples g2.s.gwa.transcripts(chr = 1L, pheno = 'FVIIIc', MAF.max = 1, normalize = TRUE,
##'     outdir = "outdir", external = FALSE, tests = NULL, verbose = TRUE)
##' 
##' @export
g2.s.gwa.transcripts <- function(chr, pheno, MAF.max = 0.05, family = 'gaussian', normalize = TRUE, outdir, visualizeResults = FALSE, external = FALSE, tests = NULL, verbose = FALSE){

    gdsfmt::showfile.gds(closeall = TRUE, verbose = FALSE)
    
    stopifnot(require(GenomicFeatures, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(TxDb.Hsapiens.UCSC.hg19.knownGene, quietly = TRUE, warn.conflicts = FALSE))

######################################################################################
    ## example::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ##  gr <- GRanges(seqnames = "chr5", strand = "-", ranges = IRanges(start = 176820000, end = 176840000))
    ##  txdb <- TxDb.Hsapiens.UCSC.hg19.knownGene
    ##    seqlevels(txdb) <- GenomeInfoDb::seqlevels0(txdb)
    ## tRNA1 <- tRNAs(txdb)
    ## chr <- 5L
    ## seqlevels(txdb, pruning.mode = "coarse") <- as.character(glue("chr{chr}"))
    ##  gen1 <- subsetByOverlaps(genes(txdb), gr, ignore.strand = FALSE)
    ## tRNA1 <- subsetByOverlaps(tRNAs(txdb), gr, ignore.strand = FALSE)
    ## gene_id of F12 is 2161
    ##  tr1 <- subsetByOverlaps(transcripts(txdb), gr, ignore.strand = FALSE) ## output coordinates for the unspliced transcript.
    ##  ex1 <- subsetByOverlaps(exons(txdb), gr, ignore.strand = FALSE)
    ##  tr2.good <- subsetByOverlaps(exonsBy(txdb, by = "tx"), gr, ignore.strand = FALSE) ## which exons makes up which transcripts.
    ##  cds1 <- subsetByOverlaps(cds(txdb), gr, ignore.strand = FALSE)
    ##  cds2.good <- subsetByOverlaps(cdsBy(txdb, by = "tx"), gr) ## the coding sequence (CDS).
    ##   lenght1 <- subset(transcriptLengths(txdb, with.cds_len = TRUE), gene_id == "2161") ## Only transcripts with length > 0 have CDS (coding sequence
    ## See [GenomicFeatures](https://kasperdanielhansen.github.io/genbioconductor/html/GenomicFeatures.html)

    ## txby <- transcriptsBy(txdb, by="gene") ## to annotate
    ## END example::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
######################################################################################
    ## See [this link for more information:](https://rdrr.io/github/smgogarten/GENESIS/man/assocTestAggregate.html)
    
    stopifnot(require(checkmate, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(purrr, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(SeqArray, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(SeqVarTools, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(gait2, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(R6, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(glue, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(GENESIS, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(VariantAnnotation, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(TxDb.Hsapiens.UCSC.hg19.knownGene, quietly = TRUE, warn.conflicts = FALSE))
    stopifnot(require(FDb.UCSC.tRNAs, quietly = TRUE, warn.conflicts = FALSE))
    checkmate::assertInteger(chr, lower = 1, upper = 22)
    checkmate::assertCharacter(pheno, len = 1)
    checkmate::assertSubset(tests, choices = c('SMMAT','SKATO','fastSKAT'), empty.ok = TRUE, fmatch = FALSE)
    checkmate::assertLogical(normalize, len = 1)
    checkmate::assertLogical(verbose, len = 1)
    checkmate::assertLogical(visualizeResults, len = 1)
    checkmate::assertCharacter(outdir, len = 1)
    dir.create(outdir, showWarnings = FALSE)
    checkmate::assertNumeric(MAF.max, min = 0, max = 1)
    checkmate::assertChoice(family,
               choices = c('gaussian','binomial', 'Gamma','inverse.gaussian', 'poisson','quasi', 'quasibinomial', 'quasipoisson'),
               null.ok = FALSE)
    if(family %in% c('binomial','quasibinomial')){
        if(normalize){
            if(verbose){
                warning('Not made sense to normalize a discrete variable')
            }
            normalize <- FALSE
        }
    }


    if(MAF.max <= 0.05){
        outfile <- glue("{outdir}/g2.r.gwa.transcripts.rare.burden.chr.{chr}.{pheno}.RData")
        outFile.df <- glue("{outdir}/g2.r.gwa.transcripts.rare.all.chr.{chr}.{pheno}.RData")
    }else{
        outfile <- glue("{outdir}/g2.r.gwa.transcripts.burden.chr.{chr}.{pheno}.RData")
        outFile.df <- glue("{outdir}/g2.r.gwa.transcripts.all.chr.{chr}.{pheno}.RData")
    }
        
    
    
    f1 <- gait2::g2.s.pheno(external = external)
    if(!f1$existTrait(pheno)){
        stop('This Trait do not exist in our DB')
    }
    covs <- f1$infoTrait(pheno)$covariates
    if(is.null(covs[1]) || is.na(covs[1]))
    {
        covs <- NULL
    }
    f1$setTraits(c(f1$getTraits('solar'), pheno, f1$getTraits('covariates')))
    dt <- f1$getData()

    gdsfile <-glue("{g2.s.dirs('gwa.1000G.gds', external = external)}/chr{chr}.gds")
    gds <- SeqArray::seqOpen(gdsfile)
    idsOrdenados <- SeqArray::seqGetData(gds, "sample.id")
    seqClose(gds)
    dt <- dt[idsOrdenados,]
    
    dt %<>% mutate(AGE = as.numeric(AGE),
                   SEX = factor(SEX))
    is.labelled <- . %>% class %>% str_detect(pattern='labelled') %>% any
    dt %<>% mutate_if( .predicate = is.labelled , .funs = as.character)
    
    if(length(covs) == 0){
        formul <- as.formula(glue("{pheno} ~ 1"))
    } else {
        formul <- as.formula(glue("{pheno} ~ {paste(covs, collapse = ' + ')}"))
    }
        
    if(normalize){
        dt[, pheno] <- dt[, pheno] %>% as.numeric %>% normalizar
    }

    if(family %in% c('binomial','quasibinomial')){
        dt[, pheno] <- dt[, pheno] %>% as.character %>% as.numeric
    }
    ## ################################################################
    ## phenotype data
    pedigree0 <- dt %>%
        mutate(sample.id = id) %>%
        plyr::rename(
                  c('FAM' = 'family',
                    'id' = 'individ',
                    'FA' = 'father',
                    'MO' = 'mother',
                    'SEX' = 'sex',
                    'AGE' = 'age') ) %>%
        dplyr::select(c(2,1,3,4,15,5:14))
    
      metadata <- data.frame(
        labelDescription=c("family id",
                           "subject id",
                           "father id",
                           "mother id",
                           "subject id",
                           "twin status",
                           "proband status",
                           "houseHold id",
                           "sex",
                           "age",
                           f1$infoTrait(pheno)$description,
                           "normalized squared age",
                           "smoking status",
                           "contraception intake",
                           "AB0 group A dominant"),
        row.names=names(pedigree0), stringsAsFactors = FALSE)
    pheno.data <- AnnotatedDataFrame(pedigree0, metadata)
    ## END phenotype data
    ## ################################################################
    ## construct a SeqVarData object
    seqData <- SeqVarTools::SeqVarData(gdsfile, sampleData= pheno.data)#, imputed=TRUE)
    
    ## ################################################################
    ## Random effects:
    ## HouseHold
    HH.mat <- g2.s.houseHold(external = external)
    HH.mat <- HH.mat[idsOrdenados, idsOrdenados]
    ## kinship
    kin <- g2.s.kin2(sparse = FALSE,external = external)
    kin <- kin %>% column_to_rownames(var='id')
    kin.mat <- kin[idsOrdenados,idsOrdenados] %>% as.matrix
    
    covMatList <- list(HH = HH.mat, kinship = kin.mat)
    ## END Random effects
    ## ################################################################
    ## fit the null model

    if(!is.null(covs)){
        covs <- covs %>% str_replace(pattern = '^AGE$','age') %>% str_replace('^SEX$','sex')
    }
    nullmod <- fitNullModel(seqData,
                            outcome=pheno,
                            covars = covs,
                            cov.mat = covMatList,
                            family = family,
                            verbose = verbose)

    txdb <- TxDb.Hsapiens.UCSC.hg19.knownGene
    
    ## keep only the selected chr
    seqlevels(txdb) <- GenomeInfoDb::seqlevels0(txdb)
    seqlevels(txdb, pruning.mode = "coarse") <- as.character(glue("chr{chr}"))
    

    ## ##############
    ## extract transcripts
    suppressMessages( gr <- genes(txdb) )
    trs.map <- subsetByOverlaps(transcripts(txdb), gr)
    trs <- subsetByOverlaps(exonsBy(txdb, by = "tx"), gr)
    ## END extract transcripts
    ## ##############

    
    ## TSS extraction
    mytrs.tss <-  resize(trs.map, width = 1, fix = 'start') %>% as.data.frame %>%
        dplyr::select(c('tx_id', TSS = 'start')) %>% mutate(tx_id = as.character(tx_id))

    seqlevelsStyle(trs) <- "NCBI"  ## seqlevelsStyle(genes) <- "UCSC"

    ## Burden ##########
    ## seqResetFilter(iterator)
    if(verbose)
        print('----------start iterator-----------')
    iterator <- SeqVarTools::SeqVarListIterator(seqData, variantRanges = trs, verbose = verbose)
    if(verbose)
        print('--------end iterator-----------')
    m.burden <- GENESIS::assocTestAggregate(
        iterator,
        nullmod,
        test="Burden",
        AF.max = MAF.max,
        genome.build = 'hg19',
        imputed = TRUE,
        sparse = FALSE,
        verbose = verbose)
    ## save(m.burden, file = outfile)

    results <- m.burden$results %>% rownames_to_column(var = 'tx_id') %>%
        dplyr::select(-Cs(Score, Score.SE, Score.Stat)) %>%
        plyr::rename(c('Score.pval' = 'pval_burden'))
    ## END Burden

    if(any(tests == 'SMMAT')){
        ## SMMAT ##########
        seqResetFilter(iterator)
        m.SMMAT <- assocTestAggregate(
            iterator,
            nullmod,
            test="SMMAT",
            AF.max = MAF.max,
            genome.build = 'hg19',
            imputed = TRUE,
            sparse = FALSE,
            verbose = verbose)
        aux <- m.SMMAT$results %>% rownames_to_column(var='tx_id') %>%
            dplyr::select(Cs(tx_id, pval_theta, pval_SMMAT))
        results %<>% left_join(aux, by = 'tx_id')
        ## END SMMAT
    }
    if(any(tests == 'SKATO')){
        ## SKATO ##########
        seqResetFilter(iterator)
        m.SKATO <- assocTestAggregate(
            iterator,
            nullmod,
            test="SKATO",
            AF.max = MAF.max,
            genome.build = 'hg19',
            imputed = TRUE,
            sparse = FALSE,
            rho=seq(0, 1, 0.25),
            verbose = verbose)
        
        aux <- m.SKATO$results %>% rownames_to_column(var='tx_id') %>%
            dplyr::select(Cs(tx_id, pval_SKATO))
        results %<>% left_join(aux, by = 'tx_id')
        ## END SKATO
    }
    
    if(any(tests == 'fastSKAT')){
        ## fastSKAT ##########
        seqResetFilter(iterator)
        m.fastSKAT <- assocTestAggregate(
            iterator,
            nullmod,
            test="fastSKAT",
            AF.max = MAF.max,
            genome.build = 'hg19',
            imputed = TRUE,
            sparse = FALSE,
            rho=seq(0, 1, 0.25),
            verbose = verbose)
        
        aux <- m.fastSKAT$results %>% rownames_to_column(var='tx_id') %>%
            dplyr::select(c('tx_id', pval_fastSKAT = pval))
        results %<>% left_join(aux, by = 'tx_id')
        ## END fastSKAT
    }

    ## add map
    aux.map <- as.data.frame(trs.map) %>% mutate(tx_id = as.character(tx_id))
    results <- results %>% left_join(aux.map, by = 'tx_id') %>% left_join(mytrs.tss, by = 'tx_id')
    results$pheno <- pheno
    save(results, file = outFile.df)
    
    seqClose(seqData)
     if(verbose){
        return(results)
    }
    return(invisible())
}



